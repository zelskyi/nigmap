""" Images models"""
from django.db import models
from accounts.models import User


def image_path(instance, filename):
    """
    Change path to saved user images
    """
    return '/'.join(['images', str(instance.user_owner.pk), filename])


class Images(models.Model):
    """
    Model for image
    """
    user_owner = models.ForeignKey(User, related_name='images')
    image = models.ImageField(upload_to=image_path)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    class Meta:
        """
        Meta Images Model
        """
        db_table = 'images'

    def __unicode__(self):
        return '%s' % (self.image)
