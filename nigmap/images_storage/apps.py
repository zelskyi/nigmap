from django.apps import AppConfig


class ImagesStorageConfig(AppConfig):
    name = 'images_storage'
