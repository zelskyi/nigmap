from django.contrib import admin
from images_storage.models import Images

class ImagesAdmin(admin.ModelAdmin):
	list_display = ['user_owner','price']



admin.site.register(Images, ImagesAdmin)