from django.forms import ModelForm, Textarea
from accounts.models import User
from mapwidgets.widgets import GooglePointFieldWidget, GoogleStaticMapWidget, GoogleStaticOverlayMapWidget
from images_storage.models import Images
from django import forms



class MarkerPositionForm(ModelForm):
    class Meta:
        model = User
        fields = ['position','first_name','last_name', 'hash_tag', 'secret']
        widgets = {
            'position': GooglePointFieldWidget,
        }


class ImageForm(forms.ModelForm):
  image = forms.ImageField( required=True, widget=forms.FileInput)

  class Meta:
        model = Images
        fields = ('image', 'price')

