from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.contrib.auth.mixins import LoginRequiredMixin
from accounts.models import User
from django.views.generic import UpdateView, CreateView, ListView
from images_storage.models import Images
from .forms import *
from django.http import Http404
from reviews.models import Review


class AddMarkerView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    form_class = MarkerPositionForm
    model = User
    success_url = "/dashboard"
    template_name = "dashboard/add_geo_form.html"

    def get_object(self, queryset=None):
        obj = User.objects.get(email=self.request.user.email)
        return obj


class AddImageView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    form_class = ImageForm
    model = Images
    success_url = "/dashboard/images"
    template_name = "dashboard/file_upload.html"

    def form_valid(self, form):
        form.instance.user_owner = self.request.user
        return super(AddImageView, self).form_valid(form)

    def images(self):
        return Images.objects.filter(user_owner=self.request.user)

    def count_images_check(self):
        count = Images.objects.filter(user_owner=self.request.user).count()
        if count < self.request.user.image_count:
            return True


class UpdateImageView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    model = Images
    form_class = ImageForm
    success_url = "/dashboard/images"
    template_name = "dashboard/update_image.html"

    def get_object(self, *args, **kwargs):
        obj = super(UpdateImageView, self).get_object(*args, **kwargs)
        if not obj.user_owner == self.request.user:
            # maybe you'll need to write a middleware to catch 403's same way
            raise Http404('Azaza suka idi nagui')
        return obj


class RatingsListView(LoginRequiredMixin, ListView):

    model = Review
    template_name = "dashboard/reviews.html"

    def get_context_data(self, **kwargs):
        context = super(RatingsListView, self).get_context_data(**kwargs)
        context.update({
            'reviews': Review.objects.filter(user=self.request.user),
        })
        return context
