from django.conf.urls import url, include
from .views import AddMarkerView, AddImageView, UpdateImageView, RatingsListView


urlpatterns = [
        url(r'^$', AddMarkerView.as_view(),  name='dashboard'),
        url(r'^images/$', AddImageView.as_view(),  name='dashboard_images'),
        url(r'^images/(?P<pk>[\w-]+)$', UpdateImageView.as_view(), name='update_image_form'),
        url(r'^reviews/$', RatingsListView.as_view(), name='reviews'),

]
