from django.db import models
from accounts.models import User
# Create your models here.


class Review(models.Model):
    user = models.ForeignKey(User,related_name='reviews')
    positive = models.BooleanField(default=False)
    ip_address = models.GenericIPAddressField()
    secret = models.CharField(max_length=20)
    checked = models.BooleanField(default=False)
    text = models.CharField(max_length=300, blank=True)

    class Meta:
        db_table = 'reviews'

