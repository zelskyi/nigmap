# Views for api app
from rest_framework import generics
from accounts.models import User
from .serializers import UserSerializer, UsersSerializer


class UserDetail(generics.RetrieveAPIView):
    """
    UserDetail api view
    """
    model = User
    serializer_class = UserSerializer
    lookup_field = 'uid'
    queryset = User.objects


class AllUsersView(generics.ListAPIView):
    """
    List data for all users
    """
    serializer_class = UsersSerializer
    queryset = User.objects.all()
