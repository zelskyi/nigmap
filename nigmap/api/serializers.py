"""Serializers for using api"""
from rest_framework import serializers
from accounts.models import User
from images_storage.models import Images
from reviews.models import Review
from drf_extra_fields.geo_fields import PointField


class ReviewSerializer(serializers.ModelSerializer):
    """
    Serialize image to belong to account
    """
    class Meta:
        """
        Import all images belong to account
        """
        model = Review
        fields = ['positive', 'text']


class ImageSerializer(serializers.ModelSerializer):
    """
    Serialize image to belong to account
    """
    class Meta:
        """
        Import all images belong to account
        """
        model = Images
        fields = ['image']


class UsersSerializer(serializers.ModelSerializer):
    """
    Send user data in to frontend.
    """
    position = PointField(required=True)
    avatar = serializers.SerializerMethodField('get_image_url')

    class Meta:
        """
        Meta class for UserSerializer
        """
        model = User
        fields = ('uid', 'avatar', 'position')

    def get_image_url(self, obj):
        """
        Retrun link to user image account(simple acc has default image)
        """
        return 'http://127.0.0.1:8000' + str(obj.avatar.url)


class UserSerializer(serializers.ModelSerializer):
    """
    Single user serializer
    """
    images = ImageSerializer(many=True)
    reviews = ReviewSerializer(many=True)


    class Meta:
        """
        Meta for UserSerializer
        """
        model = User
        fields = ('first_name', 'last_name', 'positive_reviews', 'price__avg', 'phone_number', 'images', 'reviews', 'position')
