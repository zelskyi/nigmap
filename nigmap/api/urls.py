from django.conf.urls import url, include
from .serializers import UserSerializer
from .views import UserDetail, AllUsersView


urlpatterns =(
   url(r'^user/(?P<uid>[0-9a-zA-Z_-]+)', UserDetail.as_view()),
   url(r'^users', AllUsersView.as_view()),
)