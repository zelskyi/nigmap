# Admin file 
from django.contrib import admin
from django.contrib.gis.db import models
from accounts.models import User
from mapwidgets.widgets import GooglePointFieldWidget


class UserAdmin(admin.ModelAdmin):
    """
	Display User details in admin
	"""
    list_display = ['first_name', 'phone_number', 'created_time', 'last_name', 'checked', 'uid']
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    ordering = ('-created_time',)


admin.site.register(User, UserAdmin)
