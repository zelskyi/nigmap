from django import forms
from .models import User   # fill in custom user info then save it
from django.contrib.auth.forms import UserCreationForm
import phonenumber_field
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class UserRegistrationForm(UserCreationForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField( widget=forms.PasswordInput, error_messages={'required': "Обов'язкове поле"})
    password2 = forms.CharField( widget=forms.PasswordInput, error_messages={'required': "Обов'язкове поле"})
    phone_number = phonenumber_field.modelfields.PhoneNumberField(blank=False, error_messages={'required': "Обов'язкове поле"})

    class Meta:
        model = User
        fields = ('email', 'phone_number', 'first_name', 'last_name')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Паролі не співпадають")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)

        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user












    # email = forms.EmailField(required = True)
    # first_name = forms.CharField(required = True)
    # last_name = forms.CharField(required = True)
    # phone_number = PhoneNumberField(blank=False)
    # avatar = forms.ImageField()

    # class Meta:
    #     model = User
    #     fields = ('email', 'password1', 'password2', 'phone_number')        

    # def save(self,commit = True):   
    #     user = super(UserRegistrationForm, self).save(commit = False)
    #     user.email = self.cleaned_data['email']
    #     user.first_name = self.cleaned_data['first_name']
    #     user.last_name = self.cleaned_data['last_name']
    #     user.phone_number = self.cleaned_data['phone_number']

    #     if commit:
    #         user.save()

    #     return user
