from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from accounts.forms import UserRegistrationForm
from accounts.models import User
from django.views.generic.edit import CreateView
from django.contrib.auth import authenticate, login, logout


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def logout_view(request):
    logout(request)
    return redirect('/')

def login_view(request):
    if request.POST:
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        user = authenticate(email=email, password=password)

        if user is not None:
            if user.checked:
                login(request, user)
                if login:    
                    return redirect('/dashboard')
            else:
                return render(
                request,
                template_name='accounts/not_activated.html',
            )   
              
        else:
            return render(
                request,
                template_name='accounts/login_new.html',
            )
    if request.user.is_authenticated():
        return redirect('/dashboard')

    else:
        return render(
            request,
            template_name='accounts/login_new.html',
            )

class CreateUser(CreateView):
    model = User
    form_class = UserRegistrationForm
    success_url = '/login/'

    def form_valid(self, form):
        user = form.save(self.request)
        user.set_password(form.cleaned_data.get('password'))
        user.user_ip = get_client_ip(self.request)
        user.save()
        return super(CreateUser, self).form_valid(form)
