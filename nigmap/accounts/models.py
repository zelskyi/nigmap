# Create your models here.
import uuid
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point
from django.contrib.gis.db import models
from django_resized import ResizedImageField
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from phonenumber_field.modelfields import PhoneNumberField
import images_storage
import reviews
from django.db.models import Avg


POINT = Point(-104.9903, 39.7392)


class CustomUserManager(BaseUserManager):
    """
    Custom manager for user
    """
    def create_user(self, email, first_name, last_name, password=None):
        """
        Creates and saves a User with the given email, role
        and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name='MEGA',
            last_name='GOD',
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    """
    Custom user model.
    """
    email = models.EmailField(
        verbose_name=_('Email'),
        max_length=255,
        unique=True
        )
    first_name = models.CharField(
        verbose_name=_('Імя'),
        max_length=20,
    )
    last_name = models.CharField(
        verbose_name=_('Прізвище'),
        max_length=30,
    )
    # Custom fields
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    position = models.PointField(
        help_text="To generate the map for your location",
        blank=True,
        default=POINT
        )
    avatar = ResizedImageField(blank=True, default='markers/default.png')
    image_count = models.IntegerField(default=6)
    gold = models.BooleanField(default=False)
    hash_tag = models.CharField(max_length=20, blank=True)
    checked = models.BooleanField(default=False)
    created_time = models.DateTimeField(default=timezone.now)
    secret = models.CharField(max_length=20, blank=True)
    phone_number = PhoneNumberField(blank=True)
    user_ip = models.GenericIPAddressField(default='127.0.0.1')

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def images_check(self, img_count):
        """
        @:return True if user have mutch images count. Else returns false.
        """
        if self.images == img_count:
            return True
        else:
            return False

    def is_gold(self):
        """
        @:return Check user status.
        """
        if self.gold:
            return True
        else:
            return False

    def is_checked(self, img_count):
        """
        @:return Check user active status.
        """
        if self.checked:
            return True
        else:
            return False

    @property
    def price__avg(self):
        "Count avarage price for all user images"
        price = images_storage.models.Images.objects.filter(user_owner=self).aggregate(Avg('price'))['price__avg']
        return int(price)

    @property
    def positive_reviews(self):
        "Count avarage price for all user images"
        all_reviews = reviews.models.Review.objects.filter(user=self).count()
        positive = reviews.models.Review.objects.filter(user=self).filter(positive=True).count()
        try:
            result = (positive/all_reviews)*100
            return int(result)
        except:
            return Null

    class Meta:
        db_table = 'accounts'
